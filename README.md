# Stock App
* Obligatoire d'avoir typescript installé en global sur la machine sinon faire

        npm install typescript -g 

* Installation des dependances 

        npm install

* Changer la configuration du fichier __ormconfig.json__ 
* Le serveur par defaut est sur le port 3000 sur localhost

* Lancer l'application avec :

        npm run start

# API  REST pour les produits

* Liste de tous les produits
* Information sur les produits par categorie
* creer un nouveaux produit
* effacer un produit avec son ID

**PS :** Plus d'informations sur les ressources disponibles dans le fichier __src/routes/ProductRoutes.ts__