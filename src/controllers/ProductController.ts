import {Singleton, Inject} from "typescript-ioc"
import ProductService from "../services/ProductService";
import Product from "../models/Product";
import { Context } from "koa";

enum Responce{
    HTTP_OK = 200,
    HTTP_CREATED= 201,
    HTTP_ACCEPTED= 202,
    HTTP_BAD = 400,
    HTTP_UNAUTHORIZED= 401,
    HTTP_NOT_FOUND= 404
}

@Singleton
export default class ProductController {


    constructor(@Inject private productService: ProductService){}

    public async getAllProducts(ctx:Context) : Promise<void> {
        ctx.body = await this.productService.getAllProducts()
        ctx.status=Responce.HTTP_OK
    }

    public async saveProduct(ctx:Context) : Promise<void>{
        try {
            const product : Product = Product.newProduct(ctx.request.body)
            const res = await this.productService.saveProduct(product)
            ctx.body= res
            ctx.status= Responce.HTTP_OK
            
        } catch (e) {
            ctx.throw(Responce.HTTP_BAD)
        }
    }

    public async findProduct(ctx:Context): Promise<void> {
        try {
            ctx.body =await this.productService.findProdByCategory(ctx.params.category)
            ctx.status= Responce.HTTP_OK
        } catch (e) {
            ctx.throw(Responce.HTTP_NOT_FOUND)            
        }

    }

    public async deleteProduct(ctx:Context): Promise<void>{
        try {
            ctx.body= await this.productService.deleteProduct(ctx.params.id)
            ctx.status= Responce.HTTP_OK
        } catch (error) {
            ctx.throw(Responce.HTTP_NOT_FOUND)
        }

    }



}


