import { Inject, Container } from "typescript-ioc"
import ProductRoutes from "./routes/ProductRoutes"
import { createConnection } from "typeorm"
import * as Koa from "koa"
import * as Router from "koa-router"
import * as bodyParser from "koa-bodyparser"
import * as logger from "koa-logger"
import Product from "./models/Product"

export default class StockApp {
    constructor(@Inject private ProductRoutes:ProductRoutes){}

    private async createApp() {
        await createConnection()
                // create koa App
                const app = new Koa()
                const router = new Router()

                // register all applications routes
                this.ProductRoutes.register(router)

                //run app
                app.use(logger())
                app.use(bodyParser())
                app.use(router.routes())
                app.use(router.allowedMethods())
                return Promise.resolve(app)
            }

            public async start(){
                const app = await this.createApp()
                console.log("StockApp is running on port 3000")
                const server = app.listen(3000)
                Promise.resolve(server)
    }

}


const app:StockApp = Container.get(StockApp)
app.start()