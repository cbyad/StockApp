import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export default class Product {

    @PrimaryGeneratedColumn()
    private id : number

    @Column({type:"varchar",length:50})
    private name : string
    
    @Column({type:"varchar",length:50})
    private category : string 
    
    @Column({type:"float"})
    private price : number
    
    @Column({type:"float"})
    private tva : number

    public static newProduct(obj:{id?:number,name?:string,category?:string,price?:number,tva?:number}) :Product{
        const product = new Product()
        if(obj.id) product.id = obj.id
        if(obj.name) product.name = obj.name
        if(obj.category) product.category = obj.category
        if(obj.price) product.price = obj.price
        if(obj.tva) product.tva = obj.tva
    return product
    }

    public get $id():number {
        return this.id
    } 

    public set $name(name:string)  {
        this.name =name 
    }

    public get $name():string {
        return this.name
    } 
    public set $category(cat :string )  {
        this.category =cat 
    }

    public get $category():string {
        return this.category
    } 
    public set $price(price:number)  {
        this.price = price
    }

    public get $price():number {
        return this.price
    } 
    public set $tva(tva:number)  {
        this.tva =tva 
    }

    public get $tva():number {
        return this.tva
    } 
}