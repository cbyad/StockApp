
import Route from "./Routes"
import * as Router from "koa-router"
import ProductController from "../controllers/ProductController";
import { Context } from "koa";
import { Inject } from "typescript-ioc";

export abstract class IRoutes{

    protected abstract getRoutes(): Route[]

    private registerRoute = (route: Route, router : Router)=>{
        switch (route.$method) {
            case ("get") :
                router.get(route.$path,route.$action)
                break

            case ("post"):
                router.post(route.$path,route.$action)
                break

            case ("put") :
                router.put(route.$path,route.$action)
                break

            case ("delete") :
                router.delete(route.$path,route.$action)
                break
                
            default:
                throw new Error("Invalid "+route.$method + "method")

        }
    }

    public register(router : Router): void {
        this.getRoutes().forEach( (route) => { this.registerRoute(route,router)})
    }

}

export default class ProductRoutes extends IRoutes {
    constructor(@Inject private productController: ProductController){
        super()
    }

    protected getRoutes() : Route[]{
        return [
            Route.newRoute("/products","get",(ctx:Context)=> this.productController.getAllProducts(ctx)),
            Route.newRoute("/products","post",(ctx:Context)=>this.productController.saveProduct(ctx)),  
            Route.newRoute("/products/:category","get",(ctx:Context)=>this.productController.findProduct(ctx)),
            Route.newRoute("/products/:id","delete",(ctx:Context)=>this.productController.deleteProduct(ctx))
        ]
    }





}