import { Context } from "koa";


export default class Route {

    private method : string
    private path : string
    private action: (ctx:Context)=>void

    public static newRoute(path : string, method : string, action : (ctx : Context) => void) : Route {
        const route = new Route()
        route.path=path
        route.method=method
        route.action=action
        return route
    }

    public get $path():string {
        return this.path
    }
    
    public get $method():string {
        return this.method
    }
    
    public get $action():(ctx :Context)=>void {
        return this.action
    }

}