import { getManager, Repository, QueryBuilder } from "typeorm"
import Product from "../models/Product"


export class IRepository{

    protected getProductRepository() : Repository<Product>{
        return getManager().getRepository(Product)
    }
}

export default class ProductService extends IRepository{

    public async getAllProducts() : Promise<Product[]> {
        return await this.getProductRepository().find()
    }

    
    public async findProdByCategory(category:string) : Promise<Product[]> {
       return await this.getProductRepository()
        .createQueryBuilder("product")
        .select()
        .where("product.category = :category",{category})
        .getMany()
    }

    public async saveProduct(product:Product) : Promise<Product> {
        return await this.getProductRepository().save(product)
    }

    public async deleteProduct(id:number) : Promise<void>{
        const res =await  this.getProductRepository().findOne(id)
        if(!res)
            throw new Error("Product with ID : "+id + "not found")
        
            await this.getProductRepository()
                    .createQueryBuilder("product")
                    .delete()
                    .where("product.id = :id",{id})
                    .execute()
        return Promise.resolve()
    }    
}